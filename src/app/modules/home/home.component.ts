import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  /**
   * @description redirecciona a vista de users
   */
  routeUsers(): void {
    this.router.navigate(['users']);
  }

  /**
   * @description redirecciona a vista de error al fallar la carga de la vista de users
   */
  routeUsersError(): void {
    this.router.navigate(['users'], { queryParams: { status: 'error' } });
  }

}
