import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ValidDetailUserGuard } from '../../core/guards/valid-detail-user.guard';
import { DetailUserComponent } from './components/detail-user/detail-user.component';
import { ListUsersComponent } from './components/list-users/list-users.component';


const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: ListUsersComponent },
      { path: 'detail', component: DetailUserComponent, canActivate: [ValidDetailUserGuard] },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
