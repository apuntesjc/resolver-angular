import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DetailUserStore } from '../../../../core/models/detail-user-store';

@Component({
  selector: 'app-detail-user',
  templateUrl: './detail-user.component.html',
  styleUrls: ['./detail-user.component.scss']
})
export class DetailUserComponent implements OnInit {

  constructor(
    public detailUserStore: DetailUserStore,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  /**
   * @description vuelve a listado de usuarios
   */
  backList(): void {
    this.router.navigate(['users']);
  }

}
