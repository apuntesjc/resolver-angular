import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IUser } from '../../../../core/interfaces/user.interface';
import { DetailUserStore } from '../../../../core/models/detail-user-store';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {
  public users$: Observable<IUser>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private detailUserStore: DetailUserStore,
  ) { }

  ngOnInit(): void {
    this.getUsers();
  }

  /**
   * @description se almacena observable con usuarios y se listan en html usando pipe async
   */
   getUsers(): void {
    this.users$ = this.activatedRoute.data.pipe(map(data => data.users));
  }

  /**
   * @description guarda datos del usuario y navega hasta la vista de detalle
   */
  viewDetail(user: IUser): void {
    this.detailUserStore.setDataUser = user;
    this.router.navigate(['users', 'detail']);
  }

  /**
   * @description para poder volver a la ruta de home
   */
   homeBack(): void {
    this.router.navigate(['']);
  }

}
