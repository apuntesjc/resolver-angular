interface IGeoLocation {
  lat: string;
  lng: string;
}

interface IAddressUser {
  city: string;
  geo: IGeoLocation;
  street: string;
  suite: string;
}

interface ICompanyUser {
  bs: string;
  catchPhrase: string;
  name: string;
}

export interface IUser {
  address: IAddressUser;
  company: ICompanyUser;
  email: string;
  id: number;
  name: string;
  phone: string;
  username: string;
  website: string;
}
