import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";

import { IUser } from "../interfaces/user.interface";
import { UsersService } from "../services/users.service";

@Injectable({
  providedIn: 'root'
})
export class UsersResolver implements Resolve<IUser> {

  constructor(private usersService: UsersService) {}

  // Podriamos usar --->| route: ActivatedRouteSnapshot |<--- en caso de que queramos acceder a algún parámetro
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IUser> {
    const { queryParams } = route;
    if ('status' in queryParams && queryParams.status === 'error') {
      // Para simular un error
      return this.usersService.getUsersError();
    } else {
      // A este observable se le pueden aplicar todos los métodos disponibles de rxjs (pipe, map, filter, catchError, finalize, subscribe, etc.)
      return this.usersService.getUsers();
    }
  }
}
