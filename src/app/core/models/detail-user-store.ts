import { Injectable } from "@angular/core";

import { IUser } from "../interfaces/user.interface";

@Injectable({
  providedIn: 'root'
})
export class DetailUserStore {
  private userData: IUser;

  constructor() {}

  set setDataUser(detailUser: IUser) {
    this.userData = detailUser;
  }

  get getDataUser(): IUser {
    return this.userData;
  }
}
