import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError, delay, finalize } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { IUser } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private http: HttpClient,
    private router: Router,
    private spinner: NgxSpinnerService
  ) { }

  /**
   * @description método que inicia spinner, consigue usuarios y después de 4 segundos detiene el spinner
   * @returns observable con datos de usuario
   */
  getUsers(): Observable<IUser> {
    this.spinner.show();
    return this.http.get<IUser>('https://jsonplaceholder.typicode.com/users').pipe(
      delay(4000), // Se agrega un delay de 4 segundos para simular un llamado tardido
      catchError((err: HttpErrorResponse) => {
        this.router.navigate(['error']);
        throw err;
      }),
      finalize(() => this.spinner.hide()),
    );
  }

  /**
   * @description método que inicia spinner, da error ya que no existe la ruta y después de 4 segundos detiene el spinner
   */
  getUsersError(): Observable<IUser> {
    this.spinner.show();
    return this.http.get<IUser>('http://localhost:4567').pipe(
      delay(4000), // Se agrega un delay de 4 segundos para simular un llamado tardido
      catchError((err: HttpErrorResponse) => {
        this.router.navigate(['error']);
        throw err;
      }),
      finalize(() => this.spinner.hide()),
    );
  }
}
