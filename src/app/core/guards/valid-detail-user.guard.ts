import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { DetailUserStore } from '../models/detail-user-store';

@Injectable({
  providedIn: 'root'
})
export class ValidDetailUserGuard implements CanActivate {

  constructor(
    private detailUserStore: DetailUserStore,
    private router: Router,
  ) {}

  /**
   * @description valida la existencia da datos de un usuario
   * @returns boolean que valida si puede o no ingresar a la ruta
   */
  canActivate(): boolean {
    if (!this.detailUserStore.getDataUser) {
      this.router.navigate(['']);
      return false;
    }
    return true;
  }

}
