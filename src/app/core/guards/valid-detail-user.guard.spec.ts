import { TestBed } from '@angular/core/testing';

import { ValidDetailUserGuard } from './valid-detail-user.guard';

describe('ValidDetailUserGuard', () => {
  let guard: ValidDetailUserGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ValidDetailUserGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
