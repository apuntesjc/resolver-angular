FROM node:14.18-slim AS builder

WORKDIR /app

COPY package*.json ./

RUN npm install --no-optional

RUN npm install -g @angular/cli@9.1.13

COPY . .

RUN npm run build:prod

# NGINX
FROM nginx:alpine

COPY --from=builder /app/dist/prueba  /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
